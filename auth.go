/*
Copyright 2020 Micronix Solutions, LLC (https://micronix.solutions/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package chix

import (
	"bytes"
	"errors"
	"html/template"
	"net/http"
	"net/url"

	"gitlab.com/micronix-solutions/logging"

	"github.com/go-chi/chi"
)

type AuthenticatedUser struct {
	Username  string
	Fullname  string
	Firstname string
	Lastname  string
	Email     string
}

type Authenticater interface {
	Authenticate(username string, password string) (*AuthenticatedUser, error)
}

type InMemoryAuthenticater struct {
	Credentials map[string]string
}

func (me *InMemoryAuthenticater) Authenticate(username string, password string) (*AuthenticatedUser, error) {
	pw, exists := me.Credentials[username]
	if !exists {
		return nil, errors.New("No user found for that username")
	} else if pw != password {
		return nil, errors.New("Invalid password")
	}
	return &AuthenticatedUser{username, "whatever youwant", "whatever", "youwant", "joe.user@micronix.solutions"}, nil
}

const logoutURIname = "/logout"
const loginURIname = "/login"

type authenticationHandler struct {
	sessionProvider   *SessionProvider
	logger            logging.Logger
	authProvider      Authenticater
	loginPageTemplate *template.Template
	baseURI           string
}

func (me *authenticationHandler) router(baseURI string) chi.Router {
	me.baseURI = baseURI
	r := chi.NewRouter()
	r.Get(loginURIname, me.showLogin)
	r.Post(loginURIname, me.doLogin)
	r.Get(logoutURIname, me.doLogout)

	return r
}

//ShowLogin serves a login page on the given ResponseWriter and Request
func (me *authenticationHandler) showLogin(wr http.ResponseWriter, req *http.Request) {
	me.logger.Debug("serve showLogin")
	sessionID := req.Context().Value(ContextKey("sessionID")).(string)
	session := me.sessionProvider.MustRetrieve(sessionID)
	vb := ViewBagFor(req)
	buf := new(bytes.Buffer)

	/*
		var (
			tpl *template.Template
			err error
		)

			if tpl, err = template.ParseFiles(filepath.Join("templates", "login.template.html")); err != nil {
				me.logger.Err(err.Error())
				buf.WriteString(err.Error())
				return
			}
	*/

	vb["Errors"] = session.Errors
	vb["Messages"] = session.Messages

	if err := me.loginPageTemplate.Execute(buf, vb); err != nil {
		buf.Reset()
		buf.WriteString(err.Error())
	}
	session.Errors = nil
	session.Messages = nil
	buf.WriteTo(wr)
}

func (me *authenticationHandler) doLogin(wr http.ResponseWriter, req *http.Request) {
	me.logger.Debug("serve dologin")

	sessionID := req.Context().Value(ContextKey("sessionID")).(string)
	session := me.sessionProvider.MustRetrieve(sessionID)
	user, pass := req.PostFormValue("username"), req.PostFormValue("password")

	switch user, err := me.authProvider.Authenticate(user, pass); err {
	default:
		session.Errors = append(session.Errors, err.Error())
		http.Redirect(wr, req, req.RequestURI, http.StatusFound)
	case nil:
		session.MustSetUser(user)
		next := req.URL.Query().Get("next")
		if next == "" {
			next = "/app/home"
		}
		http.Redirect(wr, req, next, http.StatusFound)
	}
}

func (me *authenticationHandler) doLogout(wr http.ResponseWriter, req *http.Request) {
	me.logger.Debug("Serve doLogout")
	sessionID, _ := req.Context().Value(ContextKey("sessionID")).(string)
	session := me.sessionProvider.MustRetrieve(sessionID)
	session.MustDeleteVal("User")
	session.Messages = append(session.Messages, "You have been logged out. See you next time!")
	http.Redirect(wr, req, me.loginURI(), http.StatusFound)
}

func (me *authenticationHandler) loginURI() string {
	return me.baseURI + loginURIname
}

func (me *authenticationHandler) logoutURI() string {
	return me.baseURI + logoutURIname
}

func (me *authenticationHandler) loginMiddleware(next http.Handler) http.Handler {

	return http.HandlerFunc(func(wr http.ResponseWriter, req *http.Request) {
		me.logger.Debug("enter LoginMiddleware")
		defer me.logger.Debug("exit LoginMiddleware")
		vb := ViewBagFor(req)
		vb["LoginURI"] = me.loginURI()
		vb["LogoutURI"] = me.logoutURI()

		var u = me.userForRequest(req)

		if u != nil {

			vb["User"] = u
			next.ServeHTTP(wr, req)

		} else {

			sessionID := req.Context().Value(ContextKey("sessionID")).(string)
			session := me.sessionProvider.MustRetrieve(sessionID)
			session.Messages = append(session.Messages, "Hi! Please log in to continue.")
			u, _ := url.Parse(me.loginURI())
			params := url.Values{}
			params.Add("next", req.RequestURI)
			u.RawQuery = params.Encode()
			http.Redirect(wr, req, u.String(), http.StatusFound)

		}
	})

}

func (me *authenticationHandler) userForRequest(req *http.Request) *AuthenticatedUser {
	sessionID := req.Context().Value(ContextKey("sessionID")).(string)
	session := me.sessionProvider.MustRetrieve(sessionID)

	var u = session.User
	if u != nil {
		return u
	}

	return nil
}

func (me *authenticationHandler) mustUserForRequest(req *http.Request) *AuthenticatedUser {
	var a = me.userForRequest(req)

	if a == nil {
		panic("No user found in the session -- probably loginMiddleware is not configured correctly")
	}

	return a
}
