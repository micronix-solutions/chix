/*
Copyright 2020 Micronix Solutions, LLC (https://micronix.solutions/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package chix

import (
	"bytes"
	"fmt"
	"html/template"
	"net/http"
	"runtime"
	"strings"
	"time"

	"gitlab.com/micronix-solutions/chix/templates"
	"gitlab.com/micronix-solutions/logging"

	"github.com/go-chi/chi"
)

// #region Types
type ContextKey string
type ViewBag map[string]interface{}

type tagTemplateDisplayNamePermissioner interface {
	tag() string
	displayName() string
	templateChain() templates.TemplateChain
	requiredPermissions() []string
}

type AppSubRoute struct {
	Tag                 string
	DisplayName         string
	URI                 string
	HttpMethods         []string
	HandlerFunc         http.HandlerFunc
	VisibleMenu         bool
	RequiredPermissions []string
	TemplateChain       templates.TemplateChain
}

func (sr *AppSubRoute) tag() string {
	return sr.Tag
}

func (sr *AppSubRoute) displayName() string {
	return sr.DisplayName
}

func (sr *AppSubRoute) templateChain() templates.TemplateChain {
	return sr.TemplateChain
}

func (sr *AppSubRoute) requiredPermissions() []string {
	return sr.RequiredPermissions
}

type AppRoute struct {
	Tag                 string
	DisplayName         string
	URI                 string
	HttpMethods         []string
	HandlerFunc         http.HandlerFunc
	VisibleMenu         bool
	RequiredPermissions []string
	TemplateChain       templates.TemplateChain
	Children            []*AppSubRoute
}

func (sr *AppRoute) tag() string {
	return sr.Tag
}

func (sr *AppRoute) displayName() string {
	return sr.DisplayName
}

func (sr *AppRoute) templateChain() templates.TemplateChain {
	return sr.TemplateChain
}

func (sr *AppRoute) requiredPermissions() []string {
	return sr.RequiredPermissions
}

func (route *AppRoute) HasVisibleChildren() bool {
	for idx := range route.Children {
		if route.Children[idx].VisibleMenu {
			return true
		}
	}

	return false
}

type ChiApplication struct {
	Logger       logging.Logger
	authHandler  *authenticationHandler
	SessProvider *SessionProvider
	permManager  *PermissionManager

	BaseURI string
	Routes  []*AppRoute
	AppName string
}

func (app *ChiApplication) Init(a Authenticater, r SessionGetterStorerDeleter, p PermissionPrincipalGetter, l logging.Logger, sessionttl time.Duration, loginPageTemplate *template.Template) {
	app.Logger = l

	app.SessProvider = NewSessionProvider(l, r, sessionttl)

	app.permManager = &PermissionManager{
		repository: p,
	}

	app.authHandler = &authenticationHandler{
		sessionProvider:   app.SessProvider,
		authProvider:      a,
		logger:            l,
		loginPageTemplate: loginPageTemplate,
	}
}

func (app *ChiApplication) Router(baseURI string) chi.Router {
	app.BaseURI = strings.TrimSuffix(baseURI, "/")
	var baseRouter = chi.NewRouter()
	//app.catchPanicMiddleware,

	baseRouter.Use(app.logRequestMiddleware, app.SessProvider.SessionMiddleware, ViewBagMiddleware, app.appGlobalMiddleware)
	baseRouter.Mount("/auth", app.authHandler.router(app.BaseURI+"/auth"))

	var appRouter = chi.NewRouter()
	appRouter.Use(app.authHandler.loginMiddleware, app.mainMenuMiddleware)

	for _, item := range app.Routes {

		var routeURI = item.URI //strings.Replace(item.URI, "//", "/", -1)
		var routeMethods = item.HttpMethods

		if len(routeMethods) < 1 {
			routeMethods = []string{http.MethodGet}
		}

		for _, mth := range routeMethods {
			appRouter.With(app.permissionsMiddleware(item), app.activeMenuItemMiddleware(item), app.renderTemplateMiddleware(item)).MethodFunc(mth, routeURI, item.HandlerFunc)
		}

		if item.Children != nil {

			for _, child := range item.Children {

				var routeURI = item.URI + child.URI //strings.Replace(item.URI + child.URI, "//", "/", -1) //strings.TrimSuffix(item.URI + child.URI, "/")
				var routeMethods = child.HttpMethods

				if len(routeMethods) < 1 {
					routeMethods = []string{http.MethodGet}
				}

				for _, mth := range routeMethods {
					appRouter.With(app.permissionsMiddleware(child), app.activeMenuItemMiddleware(child), app.renderTemplateMiddleware(child)).MethodFunc(mth, routeURI, child.HandlerFunc)
				}
			}
		}
	}

	//app.addAppRoutesToChiMux(appRouter, "", app.Routes)

	baseRouter.Mount("/", appRouter)

	baseRouter.With(app.mainMenuMiddleware).NotFound(http.HandlerFunc(app.notFound))

	return baseRouter
}

func (app *ChiApplication) catchPanicMiddleware(next http.Handler) http.Handler {

	var middleware = func(wr http.ResponseWriter, req *http.Request) {

		defer func() {

			if r := recover(); r != nil {

				var buf = make([]byte, 1<<16)
				runtime.Stack(buf, true)

				fmt.Fprintf(wr, "%s", buf)
			}

		}()

		next.ServeHTTP(wr, req)
	}

	return http.HandlerFunc(middleware)
}

// #region Request Logger
func (app *ChiApplication) logRequestMiddleware(next http.Handler) http.Handler {

	var middleware = func(wr http.ResponseWriter, req *http.Request) {

		app.Logger.Debug("Begin serve request: " + req.URL.String())
		defer app.Logger.Debug("End serve request: " + req.URL.String())

		next.ServeHTTP(wr, req)

	}

	return http.HandlerFunc(middleware)
}

// #endregion

func (app *ChiApplication) mainMenuMiddleware(next http.Handler) http.Handler {

	middleware := func(wr http.ResponseWriter, req *http.Request) {

		app.Logger.Debug("enter mainMenuMiddleware")
		defer app.Logger.Debug("exit mainMenuMiddleware")

		var vbag = ViewBagFor(req)
		var u = app.authHandler.mustUserForRequest(req)
		var items = make([]AppRoute, 0)

		for _, item := range app.Routes {

			if item.VisibleMenu && app.permManager.hasAllPermissions(u.Username, item.requiredPermissions()) {

				var i = *item

				i.URI = app.BaseURI + i.URI

				var markedChildren = make([]int, 0, 0)

				for idx, si := range i.Children {
					if !(si.VisibleMenu && app.permManager.hasAllPermissions(u.Username, si.requiredPermissions())) {
						markedChildren = append(markedChildren, idx)
					}
				}

				for ix := len(markedChildren); ix > 0; ix = len(markedChildren) {
					i.Children = append(i.Children[:markedChildren[0]], i.Children[markedChildren[0]+1:]...)

					markedChildren = markedChildren[1:]
					if len(markedChildren) > 0 {
						for idx := range markedChildren {
							markedChildren[idx] = markedChildren[idx] - 1
						}
					}
				}

				items = append(items, i)
			}

		}

		vbag["MenuItems"] = items

		next.ServeHTTP(wr, req)
	}

	return http.HandlerFunc(middleware)
}

func (app *ChiApplication) permissionsMiddleware(activeAppRoute tagTemplateDisplayNamePermissioner) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {

		middleware := func(wr http.ResponseWriter, req *http.Request) {

			app.Logger.Debug("enter permissionsMiddleware")
			defer app.Logger.Debug("exit permissionsMiddleware")
			var u = app.authHandler.mustUserForRequest(req)

			if !app.permManager.hasAllPermissions(u.Username, activeAppRoute.requiredPermissions()) {
				wr.WriteHeader(http.StatusForbidden)
				wr.Write([]byte("You don't have permission"))
				return
			}

			next.ServeHTTP(wr, req)
		}

		return http.HandlerFunc(middleware)
	}
}

func (app *ChiApplication) activeMenuItemMiddleware(activeAppRoute tagTemplateDisplayNamePermissioner) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {

		middleware := func(wr http.ResponseWriter, req *http.Request) {

			app.Logger.Debug("enter activeMenuItemMiddleware")
			defer app.Logger.Debug("exit activeMenuItemMiddleware")

			app.Logger.Debug("active menu item is " + activeAppRoute.tag())

			vbag := ViewBagFor(req)
			vbag["ActiveMenuItem"] = activeAppRoute.tag()
			vbag["PageName"] = activeAppRoute.displayName()

			next.ServeHTTP(wr, req)

		}

		return http.HandlerFunc(middleware)
	}
}

func (app *ChiApplication) renderTemplateMiddleware(menuItem tagTemplateDisplayNamePermissioner) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {

		middleware := func(wr http.ResponseWriter, req *http.Request) {

			app.Logger.Debug("enter renderTemplateMiddleware")
			defer app.Logger.Debug("exit renderTemplateMiddleware")

			next.ServeHTTP(wr, req)

			if len(menuItem.templateChain()) < 1 {
				return
			}

			var buf = new(bytes.Buffer)
			var vb = ViewBagFor(req)

			var rootTemplate = template.New(menuItem.templateChain()[0])

			var user = app.authHandler.userForRequest(req)
			if user != nil {
				var userHasPerm = func(permKey string) bool {
					return app.permManager.hasPermission(user.Username, permKey)
				}

				var sub = func(n1, n2 int) int {
					return n2 - n1
				}

				var tfm = template.FuncMap{
					"UserHasPermission": userHasPerm,
					"sub":               sub,
				}

				rootTemplate = rootTemplate.Funcs(tfm)
			}

			rootTemplate, err := rootTemplate.Parse(menuItem.templateChain()[1])

			if err != nil {

				app.Logger.Err(err.Error())
				buf.WriteString(err.Error())
			}

			//rootTemplate = rootTemplate.Funcs(funcMap)

			for _, chainTemplate := range menuItem.templateChain()[2:] {

				if _, err := rootTemplate.Parse(chainTemplate); err != nil {

					app.Logger.Err(err.Error())
					buf.WriteString(err.Error())

					return
				}
			}

			if err := rootTemplate.Execute(buf, vb); err != nil {

				buf.Reset()
				buf.WriteString(err.Error())
			}

			buf.WriteTo(wr)

		}

		return http.HandlerFunc(middleware)
	}
}

func (app *ChiApplication) appGlobalMiddleware(next http.Handler) http.Handler {

	//ensure the presence of some required ViewBag items
	var middleware = func(wr http.ResponseWriter, req *http.Request) {

		app.Logger.Debug("enter appGlobalMiddleware")
		defer app.Logger.Debug("exit appGlobalMiddleware")

		vb := ViewBagFor(req)
		vb["AppName"] = app.AppName
		vb["PageData"] = ""

		next.ServeHTTP(wr, req)
	}

	return http.HandlerFunc(middleware)
}

func (app *ChiApplication) notFound(wr http.ResponseWriter, req *http.Request) {
	app.Logger.Debug("enter notFound")
	defer app.Logger.Debug("exit notFound")

	var buf = new(bytes.Buffer)

	t, err := template.ParseFiles("templates\\404.template.html")
	if err != nil {

		app.Logger.Err(err.Error())
		buf.WriteString(err.Error())

		return
	}

	if err := t.Execute(buf, nil); err != nil {

		buf.Reset()
		buf.WriteString(err.Error())
	}

	buf.WriteTo(wr)
}

//Helper handler that can be used to redirect to the root of the app
func (app *ChiApplication) RedirectToApp(wr http.ResponseWriter, r *http.Request) {
	http.Redirect(wr, r, app.BaseURI, http.StatusMovedPermanently)
}

func NilHandlerFunc(wr http.ResponseWriter, req *http.Request) {

}
