module gitlab.com/micronix-solutions/chix

go 1.12

require (
	github.com/GeertJohan/go.rice v1.0.0
	github.com/go-chi/chi v4.1.0+incompatible
	github.com/google/uuid v1.1.1
	gitlab.com/micronix-solutions/auth v0.1.0 // indirect
	gitlab.com/micronix-solutions/httpx v0.1.0 // indirect
	gitlab.com/micronix-solutions/logging v0.2.0
)
