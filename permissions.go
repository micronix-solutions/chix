package chix

type Permission struct {
	Key         string
	Description string
}

type Role struct {
	Name        string
	Permissions []*Permission
}

type PermissionPrincipal struct {
	ID    string
	Roles []*Role
}

type PermissionPrincipalGetter interface {
	GetPermissionPrinciple(id string) *PermissionPrincipal
}

type PermissionManager struct {
	repository PermissionPrincipalGetter
}

func NewPermissionManager(repo PermissionPrincipalGetter) *PermissionManager {
	return &PermissionManager{
		repository: repo,
	}
}

func (pm *PermissionManager) hasPermission(principalID string, permissionKey string) bool {
	var p = pm.repository.GetPermissionPrinciple(principalID)

	for _, r := range p.Roles {
		for _, per := range r.Permissions {
			if per.Key == permissionKey {
				return true
			}
		}
	}

	return false
}

func (pm *PermissionManager) hasAllPermissions(principalID string, permissionKeys []string) bool {
	for _, pk := range permissionKeys {
		if !pm.hasPermission(principalID, pk) {
			return false
		}
	}

	return true
}

type InMemoryPrincipleRepository map[string]*PermissionPrincipal

func (imp InMemoryPrincipleRepository) GetPermissionPrinciple(id string) *PermissionPrincipal {
	return imp[id]
}
