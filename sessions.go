/*
Copyright 2020 Micronix Solutions, LLC (https://micronix.solutions/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package chix

import (
	"context"
	"math"
	"net/http"
	"time"

	"github.com/google/uuid"
	"gitlab.com/micronix-solutions/logging"
)

// #endregion

// #endregion

// #region Sessions
type SessionDataItem struct {
	StringVal string
	IntVal    int
	BoolVal   bool
}

type Session struct {
	key        uuid.UUID
	repository SessionGetterStorerDeleter
	Expiration time.Time
	User       *AuthenticatedUser
	Data       map[string]SessionDataItem
	Messages   []string
	Errors     []string
}

func newSession(key uuid.UUID, repository SessionGetterStorerDeleter, expiration time.Time, initData map[string]SessionDataItem) *Session {
	var s = &Session{
		key:        key,
		repository: repository,
		Expiration: expiration,
		Data:       initData,
		Messages:   make([]string, 0),
		Errors:     make([]string, 0),
	}

	if s.Data == nil {
		s.Data = make(map[string]SessionDataItem)
	}

	return s
}

func (s *Session) SetUser(u *AuthenticatedUser) error {
	s.User = u
	return s.repository.StoreSession(s.key, s)
}

func (s *Session) MustSetUser(u *AuthenticatedUser) {
	if err := s.SetUser(u); err != nil {
		panic(err)
	}
}

func (s *Session) PushMessage(message string) error {
	s.Messages = append(s.Messages, message)
	return s.repository.StoreSession(s.key, s)
}

func (s *Session) MustPushMessage(message string) {
	if err := s.PushMessage(message); err != nil {
		panic(err)
	}
}

func (s *Session) PopMessage() (string, error) {
	var msg = s.Messages[len(s.Messages)-1]
	var newMsgMax = int(math.Max(0, float64(len(s.Messages)-1)))
	s.Messages = s.Messages[0:newMsgMax]

	return msg, s.repository.StoreSession(s.key, s)
}

func (s *Session) MustPopMessage() string {
	msg, err := s.PopMessage()
	if err != nil {
		panic(err)
	}
	return msg
}

func (s *Session) PushError(err string) error {
	s.Errors = append(s.Errors, err)
	return s.repository.StoreSession(s.key, s)
}

func (s *Session) MustPushError(err string) {
	if err := s.PushError(err); err != nil {
		panic(err)
	}
}

func (s *Session) PopError() (string, error) {
	var err = s.Errors[len(s.Errors)-1]
	var newErrMax = int(math.Max(0, float64(len(s.Errors)-1)))
	s.Errors = s.Errors[0:newErrMax]

	return err, s.repository.StoreSession(s.key, s)
}

func (s *Session) MustPopError() string {
	errString, err := s.PopError()
	if err != nil {
		panic(err)
	}

	return errString
}

func (s *Session) StoreVal(key string, val SessionDataItem) error {
	s.Data[key] = val

	return s.repository.StoreSession(s.key, s)
}

func (s *Session) MustStoreVal(key string, val SessionDataItem) {
	if err := s.StoreVal(key, val); err != nil {
		panic(err)
	}
}

func (s *Session) GetVal(key string) interface{} {
	return s.Data[key]
}

func (s *Session) DeleteVal(key string) error {
	delete(s.Data, key)

	return s.repository.StoreSession(s.key, s)
}

func (s *Session) MustDeleteVal(key string) {
	if err := s.DeleteVal(key); err != nil {
		panic(err)
	}
}

type SessionGetterStorerDeleter interface {
	GetAllSessions() ([]*Session, error)
	GetSession(key uuid.UUID) (*Session, error)
	StoreSession(key uuid.UUID, sess *Session) error
	DeleteSession(key uuid.UUID) error
}

/*
type InMemorySessionRepository struct {
	sessions map[uuid.UUID]*Session
}
*/

type InMemorySessionRepository map[uuid.UUID]*Session

func (sr InMemorySessionRepository) GetAllSessions() ([]*Session, error) {
	var s = make([]*Session, 0, len(sr))

	for _, v := range sr {
		s = append(s, v)
	}

	return s, nil
}

func (sr InMemorySessionRepository) GetSession(key uuid.UUID) (*Session, error) {
	//sr.ensureSessionStore()
	return sr[key], nil
}

func (sr InMemorySessionRepository) StoreSession(key uuid.UUID, sess *Session) error {
	//sr.ensureSessionStore()
	sr[key] = sess

	return nil
}

func (sr InMemorySessionRepository) DeleteSession(key uuid.UUID) error {
	delete(sr, key)

	return nil
}

/*
func (sr *InMemorySessionRepository) ensureSessionStore() {
	if sr.sessions == nil {
		sr.sessions = make(map[uuid.UUID]*Session)
	}
}
*/

type SessionProvider struct {
	Logger     logging.Logger
	Repository SessionGetterStorerDeleter
	sessionTTL time.Duration
}

func NewSessionProvider(logger logging.Logger, repository SessionGetterStorerDeleter, sessionTTL time.Duration) *SessionProvider {
	var t = time.NewTicker(sessionTTL)
	go func() {
		for range t.C {
			sessions, err := repository.GetAllSessions()
			if err != nil {
				panic(err)
			}

			for _, s := range sessions {
				if s.Expiration.Before(time.Now()) {
					repository.DeleteSession(s.key)
				}
			}
		}
	}()

	return &SessionProvider{
		Repository: repository,
		sessionTTL: sessionTTL,
		Logger:     logger,
	}
}

func (me *SessionProvider) Retrieve(id string) (*Session, error) {
	uid, err := uuid.Parse(id)
	if err != nil {
		return nil, err
	}
	s, err := me.Repository.GetSession(uid)
	if err != nil {
		return nil, err
	}

	if s != nil {
		s.repository = me.Repository
		s.key = uid
	}

	return s, nil
}

func (me *SessionProvider) MustRetrieve(id string) *Session {
	session, err := me.Retrieve(id)
	if err != nil {
		panic(err)
	}

	return session
}

func (me *SessionProvider) NewSession(data map[string]SessionDataItem) (string, *Session, error) {
	var sessid = uuid.New()
	var session = newSession(sessid, me.Repository, time.Now().Add(me.sessionTTL), data)

	return sessid.String(), session, me.Repository.StoreSession(sessid, session)
}

func (me *SessionProvider) MustNewSession(data map[string]SessionDataItem) (string, *Session) {
	sessid, session, err := me.NewSession(data)

	if err != nil {
		panic(err)
	}

	return sessid, session
}

func (me *SessionProvider) DeleteSession(id string) error {
	sessid, err := uuid.Parse(id)
	if err != nil {
		return err
	}

	return me.Repository.DeleteSession(sessid)
}

func (me *SessionProvider) MustDeleteSession(id string) {
	if err := me.DeleteSession(id); err != nil {
		panic(err)
	}
}

func (me *SessionProvider) SetSessionTTL(ttl time.Duration) {
	me.sessionTTL = ttl
}

func (me *SessionProvider) SessionTTL() time.Duration {
	return me.sessionTTL
}

func (me *SessionProvider) SessionMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(wr http.ResponseWriter, req *http.Request) {
		me.Logger.Debug("enter SessionMiddleware")
		defer me.Logger.Debug("exit SessionMiddleware")
		sessionID, session := me.extractSessionFromRequest(req)
		if session == nil || session.Expiration.Before(time.Now()) {
			me.Logger.Debug("No / expired session found for cookie value")
			sessionID, session = me.MustNewSession(nil)
			http.SetCookie(wr, &http.Cookie{
				Name:   "sessid",
				Path:   "/",
				Value:  sessionID,
				MaxAge: int(me.SessionTTL()),
			})
		}
		session.Expiration = time.Now().Add(me.SessionTTL())
		next.ServeHTTP(wr, req.WithContext(context.WithValue(req.Context(), ContextKey("sessionID"), sessionID)))
	})
}

func (me *SessionProvider) extractSessionFromRequest(req *http.Request) (string, *Session) {
	me.Logger.Debug("enter extractSessionIDFromRequest")
	defer me.Logger.Debug("exit extractSessionIDFromRequest")

	c, err := req.Cookie("sessid")
	if err != nil { //no session cookie set
		me.Logger.Debug(err.Error())
		return "", nil
	}

	return c.Value, me.MustRetrieve(c.Value)
}

// #endregion
