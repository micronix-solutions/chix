package templates

import (
	"html/template"

	rice "github.com/GeertJohan/go.rice"
)

type TemplateChain []string

type layoutFetcher func() string

var templateBox *rice.Box
var isCustomTemplateBox bool

func SetTemplateBox(b *rice.Box) {
	templateBox = b
	isCustomTemplateBox = true
	frameTemplate = TemplateChain{frameTemplateFile, templateBox.MustString(frameTemplateFile)}
	modalTemplate = TemplateChain{modalTemplateFile, templateBox.MustString(modalTemplateFile)}
}

func TemplateBox() *rice.Box {
	if isCustomTemplateBox == false && templateBox == nil {
		templateBox = rice.MustFindBox("builtin")
	}

	return templateBox
}

var PageLayouts = struct {
	ListFilterLayout layoutFetcher
	FormLayout       layoutFetcher
}{
	ListFilterLayout: func() string { return TemplateBox().MustString("listfilter.template.html") },
	FormLayout:       func() string { return TemplateBox().MustString("formview.template.html") },
}

var frameTemplateFile = "frame.template.html"
var modalTemplateFile = "modal.template.html"
var loginTemplateFile = "login.template.html"

func SetBasePageTemplateFile(f string) {
	frameTemplateFile = f
}

func SetModalTemplateFile(f string) {
	modalTemplateFile = f
}

func SetLoginFile(f string) {
	loginTemplateFile = f
}

var frameTemplate TemplateChain
var modalTemplate TemplateChain

func PageLayoutTemplate(layouts ...string) TemplateChain {
	if frameTemplate == nil {
		frameTemplate = TemplateChain{frameTemplateFile, TemplateBox().MustString(frameTemplateFile)}
	}

	return append(frameTemplate, layouts...)
}

func ModalLayoutTemplate(layouts ...string) TemplateChain {
	if modalTemplate == nil {
		modalTemplate = TemplateChain{modalTemplateFile, TemplateBox().MustString(modalTemplateFile)}
	}

	return append(modalTemplate, layouts...)
}

func LoginTemplate() *template.Template {
	var loginPageTemplate = template.New(loginTemplateFile)
	loginPageTemplate, err := loginPageTemplate.Parse(TemplateBox().MustString(loginTemplateFile))
	if err != nil {
		panic(err)
	}

	return loginPageTemplate
}
