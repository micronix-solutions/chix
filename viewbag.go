package chix

import (
	"context"
	"net/http"
)

func ViewBagMiddleware(next http.Handler) http.Handler {
	attachViewBag := func(wr http.ResponseWriter, req *http.Request) {
		//logger.Debug("enter viewBagMiddleware")
		//defer logger.Debug("exit viewBagMiddleware")
		newContext := context.WithValue(req.Context(), ContextKey("ViewBag"), make(ViewBag))
		newReq := req.WithContext(newContext)
		next.ServeHTTP(wr, newReq)
	}

	return http.HandlerFunc(attachViewBag)
}

func ViewBagFor(req *http.Request) ViewBag {
	return req.Context().Value(ContextKey("ViewBag")).(ViewBag)
}
